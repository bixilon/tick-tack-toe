package de.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

	static List<Button> buttons = new ArrayList<Button>();

	static TextArea t1;
	static TextArea t2;
	static String player_chars[] = { "X", "O" };

	static int turn = 0;

	static int current_player = 1;

	public static void main(String[] args) {
		launch(args);
	}

	static int i = 0;

	@Override
	public void start(Stage stage) throws IOException {
		// Create the FXMLLoader
		FXMLLoader loader = new FXMLLoader();
		// Path to the FXML File
		String fxmlDocPath = "scene.fxml";
		FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);

		// Create the Pane and all Details
		VBox root = (VBox) loader.load(fxmlStream);

		// Create the Scene
		Scene scene = new Scene(root);
		// Set the Scene to the Stage
		stage.setScene(scene);
		// Set the Title to the Stage
		stage.setTitle("Tick Tack Toe");
		// Display the Stage
		stage.show();

		for (i = 0; i < 9; i++) {
			buttons.add(i, (Button) scene.lookup("#b" + (i)));
			buttons.get(i).setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					button_clicked(((Button) e.getSource()).getId().charAt(1) - 48);
				}
			});

		}
		clear_buttons();

		t1 = (TextArea) scene.lookup("#t1");
		t2 = (TextArea) scene.lookup("#t2");

		// setting all events:
	}

	static void button_clicked(int id) {
		turn++;
		Button pressed = buttons.get(id);
		pressed.setText(player_chars[current_player - 1]);
		pressed.setDisable(true);

		change_text(current_player);
		check_if_player_won();
		current_player = get_next_player();

	}

	static void change_text(int player) {
		t2.setText("Spieler " + player + "...");
	}

	static int get_next_player() {
		if (current_player == 1)
			return 2;
		else
			return 1;
	}

	static void check_if_player_won() {
		boolean won = false;
		if (pob(0) && pob(1) && pob(2))
			won = true;
		else if (pob(3) && pob(4) && pob(5))
			won = true;
		else if (pob(6) && pob(7) && pob(8))
			won = true;
		else if (pob(0) && pob(3) && pob(6))
			won = true;
		else if (pob(1) && pob(4) && pob(7))
			won = true;
		else if (pob(2) && pob(5) && pob(8))
			won = true;
		else if (pob(2) && pob(4) && pob(6))
			won = true;
		else if (pob(0) && pob(4) && pob(8))
			won = true;
		if (!won && turn == 9) {
			// Unentschieden
			draw();
		}
		if (won)
			win(current_player);

	}

	static boolean pob(int button) {
		if (buttons.get(button).getText() == player_chars[current_player - 1])
			return true;
		return false;
	}

	static void draw() {
		t1.setText("Unentschieden!");
		next_game();
	}

	static void win(int player) {
		t1.setText("Spieler " + current_player + " hat gewonnen!");
		next_game();
	}

	static void next_game() {
		turn = 0;
		t2.setText("Warten auf neue Runde...");
		disable_buttons();

		// Delay 3 secs
		Timer timer = new java.util.Timer();

		timer.schedule(new TimerTask() {
			public void run() {
				Platform.runLater(new Runnable() {
					public void run() {
						clear_buttons();
						change_text(current_player);
						t1.setText("Spiel im Gange...");
					}
				});
			}
		}, 3000);

	}

	static void clear_buttons() {
		for (int i = 0; i < 9; i++) {
			buttons.get(i).setText("-");
			buttons.get(i).setDisable(false);
		}
	}

	static void disable_buttons() {
		for (int i = 0; i < 9; i++) {
			buttons.get(i).setDisable(true);
		}
	}
}